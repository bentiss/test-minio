#!/usr/bin/env python3

import boto3
import click
import json
import shutil

from botocore.client import Config
from pathlib import Path


@click.group()
def cli():
    pass


@cli.command()
@click.option('--endpoint_url',
              default='http://minio-packet.freedesktop.org:9000',
              help='The minio instance to contact')
@click.argument('token')
def login(endpoint_url, token):
    '''Login to the minio server'''
    session = boto3.Session()
    sts = session.client('sts',
                         endpoint_url=endpoint_url,
                         config=Config(signature_version='s3v4'),
                         region_name='us-east-1')

    roleArn = 'arn:aws:iam::123456789012:role/FederatedWebIdentityRole'
    ret = sts.assume_role_with_web_identity(DurationSeconds=900,
                                            WebIdentityToken=token,
                                            RoleArn=roleArn,
                                            RoleSessionName='session_name')
    creds = ret['Credentials']
    creds['endpoint_url'] = endpoint_url
    creds['Expiration'] = creds['Expiration'].isoformat()
    with open('.minio_credentials', 'w') as outfile:
        json.dump(creds, outfile)


class S3(object):
    def __init__(self, credentials):
        with open(credentials) as credfile:
            creds = json.load(credfile)

        self.s3 = boto3.resource('s3',
                                 endpoint_url=creds['endpoint_url'],
                                 aws_access_key_id=creds['AccessKeyId'],
                                 aws_secret_access_key=creds['SecretAccessKey'],
                                 aws_session_token=creds['SessionToken'],
                                 config=Config(signature_version='s3v4'),
                                 region_name='us-east-1')

    def get_objects(self, path_str, expand=True):
        buckets = list(b.name for b in self.s3.buckets.all())

        bucket = path_str
        obj = ''

        if '/' in path_str:
            bucket, obj = path_str.split('/', 1)

        # if the bucket doesn't exist, we are using local files
        if bucket not in buckets:
            p = Path(path_str)
            if p.is_dir() and expand:
                return None, list(p.glob('*'))

            if p.exists():
                return None, [p]

            return None, []

        b = self.s3.Bucket(bucket)
        objs = list(o.key for o in b.objects.all())

        # easy: the object we reference exists and is not a directory
        if obj in objs:
            return b, [Path(obj)]

        if not obj.endswith('/'):
            obj += '/'

        if expand:
            obj = obj.lstrip('/')

            # find all entries below the current dir
            matches = [Path(o) for o in objs if o.startswith(obj)]

            obj = Path(obj)

            result = []

            # strip out the subdir entries
            # but keep the subdir themselves
            for m in matches:
                cur = m

                while cur.parent != Path(obj):
                    cur = cur.parent

                # this is a file in the current dir
                if cur == m:
                    result.append(m)

                # first time we see that directory
                if cur not in result:
                    result.append(cur)

            return b, result

        return b, [Path(obj)]


@cli.command()
@click.option('--credentials',
              default='.minio_credentials',
              type=click.Path(exists=True))
@click.argument('path', default='.')
def ls(path, credentials):
    s3 = S3(credentials)
    bucket, objs = s3.get_objects(path)

    for o in objs:
        print(o.name)


@cli.command()
@click.option('--credentials',
              default='.minio_credentials',
              type=click.Path(exists=True))
@click.argument('src')
@click.argument('dst')
def cp(src, dst, credentials):
    s3 = S3(credentials)

    src_bucket, src_objs = s3.get_objects(src)
    dst_bucket, dst_objs = s3.get_objects(dst, expand=False)

    # is src is a folder, complain
    if len(src_objs) > 1:
        raise ValueError('can not do recursive cp')

    # src doesn't exist
    if not src_objs:
        raise FileNotFoundError(src)

    # dst doesn't exist:
    # dst_objs can not be empty:
    #  - if we are looking at local file, empty means file not found
    #    -> re-create the posix path
    #  - for remote files, we should get at least '/'
    if not dst_objs:
        dst_objs = [Path(dst)]

    src_obj = src_objs[0]
    dst_obj = dst_objs[0]

    # Fail if both path are remote
    if src_bucket is not None and dst_bucket is not None:
        raise ValueError('at least one argument must be a local path')

    # local
    if src_bucket is None and dst_bucket is None:
        shutil.copy(src_obj, dst)
        return

    if not dst_obj.name:
        dst_obj = src_obj.name

    # upload
    if src_bucket is None:
        dst_bucket.upload_file(str(src_obj), str(dst_obj))

    # download
    else:
        src_bucket.download_file(str(src_obj), str(dst_obj))


if __name__ == '__main__':
    cli()
